"""
Run the program by calling python fingerprint.py
I used the python fingerprint.py training to find good parameters.

Requires scipy and numpy.
"""
import csv
import sys
import numpy as np
import scipy.stats as stats
np.set_printoptions(threshold=np.nan)
import itertools

class FingerPrintPositioning:

    def __init__(self):
        with open("training_set.csv") as f:
            data = np.array(list(csv.reader(f, quoting=csv.QUOTE_NONNUMERIC))).astype('float')
            data[data == 0] = np.nan

        self.keys, grouped_data = self._group_data_by_point(data)

        column_count = len(grouped_data[0][0])

        # Calculate the average values for each sensor on each point.
        averages = np.nanmean(grouped_data, axis=1) # Skipping the zeroes
        averages[np.isnan(averages)] = 0

        # Calculate the average standard deviations for each sensor on each point.
        std = np.nanstd(grouped_data, axis=1)
        std[np.isnan(std)] = 0 # Skipping the zeroes
        std[std==0] = 10000 # No measurements, so we "ignore" the sensor.

        # Substitute zeroes in the data with averages
        for point, group in enumerate(grouped_data):
            for row in group:
                for i in range(column_count):
                    if np.isnan(row[i]):
                        row[i] = averages[point][i]

        # Calculate normal distributions based on the averages and normal distributions
        self.distributions = []
        for i in range(len(grouped_data)):
            group_distributions = []
            for j in range(column_count):
                group_distributions.append(stats.norm(averages[i][j], std[i][j]))
            self.distributions.append(group_distributions)

    def _group_data_by_point(self, data):
        grouped_data = []
        keys = []
        for key, group in itertools.groupby(data, key=lambda row: row[-1]):
            group_without_key = [row[:-1] for row in group]
            grouped_data.append(group_without_key)
            if key not in keys:
                keys.append(key)

        return keys, np.array(grouped_data)

    def predict(self, sensor_data):
        """
        Take the sensor readings and calculate the probabilities for each point ignoring
        any zero-valued readings.
        """
        probs = []
        for i in range(len(self.distributions)):
            prob = 1
            for j in range(len(sensor_data)):
                if sensor_data[j] != 0:  # Ignore the sensors that did not receive anything.
                    prob *= self.distributions[i][j].pdf(sensor_data[j])
            probs.append(prob)
        return self.keys[np.argmax(probs)]



if __name__ == '__main__':
    model = FingerPrintPositioning()
    if len(sys.argv) > 1 and sys.argv[1] == 'training':
        with open('training_set.csv') as f:
            data = np.array(list(csv.reader(f, quoting=csv.QUOTE_NONNUMERIC))).astype('float')
            for row in data:
                if(model.predict(row[:-1]) == row[-1]):
                    print("Success")
                else:
                    print("Fail")
    else:
        with open('testing_set.csv') as f:
            data = np.array(list(csv.reader(f, quoting=csv.QUOTE_NONNUMERIC))).astype('float')
            for row in data:
                print(model.predict(row))
