import numpy as np
np.set_printoptions(suppress=True, threshold=1000000)
from xml.etree import ElementTree as ET

class LocationData:

    def __init__(self, location_data, destination_data_filepath):
        self.location_data = location_data
        self.destination_data_filepath = destination_data_filepath

        root = ET.Element('kml', {"xmlns": "http://www.opengis.net/kml/2.2"})
        self.tree = ET.ElementTree(root)
        self.folder = ET.Element('Folder')
        root.append(self.folder)

    def generate_basic_kml(self):
        for location in self.location_data:
            placemark = ET.Element("Placemark")
            point = ET.Element("Point")
            coordinates = self.coordinates_element([location])

            point.append(coordinates)
            placemark.append(point)
            self.folder.append(placemark)
        self.write()

    def generate_line_kml(self):
        placemark = ET.Element("Placemark")
        line_string = ET.Element("LineString")

        line_string.append(self.coordinates_element(self.location_data))
        placemark.append(line_string)
        self.folder.append(placemark)
        self.write()

    def add_gaussian_noise(self):
        average = 0
        standard_deviation = 0.01
        self.location_data = [(random.gauss(average, standard_deviation) + float(lat),
                               random.gauss(average, standard_deviation) + float(lng))
                              for lat, lng in self.location_data]


    def coordinates_element(self, coordinate_list):
        coordinates = ET.Element("coordinates")
        coordinates.text = "\n".join(["{},{},0".format(lat, lng) for lat, lng in coordinate_list])
        return coordinates

    def write(self):
        self.tree.write(self.destination_data_filepath, xml_declaration=True, encoding="UTF-8")

if __name__ == '__main__':
    max_hdop = 3
    min_satellites = 4
    max_velocity = 0.1

    data = np.genfromtxt("tokyo.csv", delimiter=',')
    LocationData(data[:, 0:2], 'unfiltered.kml').generate_basic_kml()
    data = data[data[:, 3] >= min_satellites]
    LocationData(data[:, 0:2], 'satellites.kml').generate_basic_kml()
    data = data[data[:, 4] <= max_hdop]
    LocationData(data[:, 0:2], 'hdop.kml').generate_basic_kml()

    dataT = np.transpose(data[:, 0:2])
    dataDelta = np.diff(dataT)
    dataPowers = np.power(dataDelta, 2)
    dataSummed = dataPowers[0] + dataPowers[1]
    dataSquared = np.sqrt(dataSummed)
    dataMeters = dataSquared * 100000

    time = data[:, 2]
    timeDelta = np.diff(time)

    velocity = dataMeters / timeDelta

    data[1:, -1] = velocity
    data = data[data[:, -1] <= max_velocity]

    LocationData(data[:, 0:2], 'velocity_pruning.kml').generate_basic_kml()

