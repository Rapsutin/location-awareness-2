import os
import pickle
import collections
import itertools
import numpy as np

class Edit:
    def __init__(self, path1, path2):
        self.path1 = path1
        self.path2 = path2
        self.memory = collections.defaultdict(dict)

    def edit(self, i, j):
        if str(i) in self.memory and str(j) in self.memory[str(i)]:
            return self.memory[str(i)][str(j)]

        if i == 0:
            return j

        if j == 0:
            return i

        if (self.path1[i] == self.path2[j]):
            return self.edit(i - 1, j - 1)

        result = min(self.edit(i - 1, j), self.edit(i, j - 1), self.edit(i - 1, j - 1)) + 1
        self.memory[str(i)][str(j)] = result
        return result

def edit_helper(path1, path2):
    return Edit(path1, path2).edit(len(path1) - 1, len(path2) - 1)

def similarities(paths):
    all_combinations = list(itertools.combinations(paths.keys(), 2))
    counter = 0
    similarities = np.zeros((50, 50), dtype=float)
    for i in range(len(similarities)):
        similarities[i][i] = 100
    for a, b in all_combinations:
        counter += 1
        a_index = int(a.split('.')[0]) - 1
        b_index = int(b.split('.')[0]) - 1
        similarity = max(len(paths[a]), len(paths[b]))/max(edit_helper(paths[a], paths[b]), 1)
        similarities[a_index][b_index] = similarity
        similarities[b_index][a_index] = similarity

    with open('similarities4.pkl', 'wb') as f:
        pickle.dump(similarities, f)



paths = {}
for filename in os.listdir('paths_clean'):
    paths[filename] = np.genfromtxt('paths_clean/' + filename, delimiter=',')[:, -1]
similarities(paths)
