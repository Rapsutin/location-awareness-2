import sys
import csv
import os
import numpy as np

def clean(filename):
    cleaned = []
    with open(filename) as f:
        readings = list(csv.reader(f))
        previous_id = None
        for reading in readings:
            if reading[-1] != previous_id:
                cleaned.append(reading)

            previous_id = reading[-1]

    with open("paths_clean/" + os.path.basename(filename), 'w') as f:
        csv.writer(f).writerows(cleaned)

for filename in sys.argv[1:]:
    print(filename)
    clean(filename)
