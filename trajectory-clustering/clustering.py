import csv
import pprint
import os
import sys
import itertools
import collections
import pickle
import random
from matplotlib import pyplot

import numpy as np
np.set_printoptions(threshold=10000000)

def new_medoids(clusters, similarities):
    new_medoids = []
    for cluster_points in clusters.values():
        centralness_values = {}
        for point in cluster_points:
            centralness_values[point] = sum(similarities[point][p] for p in cluster_points)
        most_central_point = max(centralness_values.items(), key=lambda x: x[1])[0]
        new_medoids.append(most_central_point)

    return new_medoids

def calculate_clusters(medoids, similarities):
    clusters = collections.defaultdict(list)
    for point in range(50):

        if point in medoids:
            clusters[point].append(point)
            continue

        best_medoid = None
        best_similarity = -10000
        for medoid in medoids:
            similarity = similarities[point][medoid]

            if similarity > best_similarity:
                best_medoid = medoid
                best_similarity = similarity

        clusters[best_medoid].append(point)
    return clusters


with open('similarities4.pkl', 'rb') as f:
    similarities = pickle.load(f)

# Calculate simple K-medoids.
k = sys.argv[1]
medoids = random.sample(range(50), int(k))

similarity = 0
while True: # Continue until the clusters stop becoming better.
    clusters = calculate_clusters(medoids, similarities)
    medoids = new_medoids(clusters, similarities)

    new_similarity = 0
    for medoid, nodes in clusters.items():
        new_similarity += sum(similarities[medoid][node] for node in nodes)
    if new_similarity > similarity:
        similarity = new_similarity
        continue
    else:
        print(similarity)
        break



for medoid, cluster in clusters.items():
    figure = pyplot.figure()
    pyplot.clf()

    for i in range(len(cluster)):
        data = np.genfromtxt('paths_clean/{}.csv'.format(cluster[i] + 1), delimiter=',')
        pyplot.plot(data[:, 1], data[:, 2])
        pyplot.axis([0, 1000, 0, 2500])
        pyplot.savefig("images/{}_{}.png".format(medoid, cluster[i] + 1))
        pyplot.clf()



    print(cluster)










