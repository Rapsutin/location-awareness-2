import numpy as np
from scipy.stats.mstats import normaltest

def analyze(filename):
    print(filename)
    data = np.genfromtxt(filename, delimiter=',')
    windowed_data = np.array_split(data, 3)
    for window in windowed_data:
        print(np.average(window, axis=0))
    print()


analyze("mode1.csv")
analyze("mode2.csv")
