import numpy as np
np.set_printoptions(suppress=True)


data = np.genfromtxt("gps.csv", delimiter=',')
distance_of_segment = np.sum(data[:, 2])/1000 # In kilometers

heading_differences = np.diff(data[:, 3])
over_threshold = len(heading_differences[heading_differences > 40])
print("HCR (changes/km):", over_threshold/distance_of_segment)

velocity_differences = np.diff(data[:, 4])
over_threshold = len(velocity_differences[velocity_differences > 0.004])
print("VCR (changes/km)", over_threshold/distance_of_segment)

velocities = data[:, 4]
stops = len(velocities[velocities < 0.015])
print("SR (stops/km)", stops/distance_of_segment)




