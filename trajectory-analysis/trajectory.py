import numpy as np
import math

data = np.transpose(np.genfromtxt("trajectory.csv", delimiter=','))
print(np.interp(1289921370.5, data[2], data[0]), np.interp(1289921370.5, data[2], data[1]))
print(np.interp(1289921576.72, data[2], data[0]), np.interp(1289921576.72, data[2], data[1]))

def distance_from_line(p, start, end):
    parallel_vector = end - start
    parallel_unit_vector = parallel_vector / np.linalg.norm(parallel_vector)

    # https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line#Vector_formulation
    a = start
    n = parallel_unit_vector
    distance = np.linalg.norm((a - p) - (np.dot(a - p, n)) * n)
    return distance

data = np.transpose(data)[:, :2]
end_points = [0, len(data) - 1]

while True:
    max_error = 0
    max_error_index = 0
    for i in range(1, len(end_points)):
        line_start = data[end_points[i-1]]
        line_end = data[end_points[i]]
        for point_index in range(end_points[i-1], end_points[i]):
            point = data[point_index]
            error = distance_from_line(point, line_start, line_end)
            if error > max_error:
                max_error = error
                max_error_index = point_index

    if(max_error < 0.0045): # Error at most 500 meters
        print(max_error, end_points)
        break

    end_points.append(max_error_index)
    end_points.sort()






