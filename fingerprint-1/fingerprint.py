import math
DATA = [
    [-60, -62, 36.41, 211.4], [-80, -90, 84.21, 99.1],
    [-66, -71, 35.99, 179.99], [-49, -66, 55.21, 223.78],
    [-91, -93, 55.98, 109.11], [-50, -81, 85.61, 223.98],
    [-72, -55, 22.57, 197.91], [-83, -79, 53.33, 141.12],
    [-67, -61, 22.51, 226.1], [-86, -91, 82.11, 128.97]
]


def euclidean(ss1, ss2, a):
    return math.sqrt((ss1 - a[0])**2 + (ss2 - a[1])**2)


def k_nearest(ss1, ss2, k):
    return sorted(DATA, key=lambda point: euclidean(ss1, ss2, point))[:k]


def average_location(points):
    return sum(point[2] for point in points) / len(points), sum(
        point[3] for point in points) / len(points)


def weighted_average_location(ss1, ss2, points):
    distances = [euclidean(ss1, ss2, point) for point in points]
    inverse_distances = sum(1/euclidean(ss1, ss2, point) for point in points)

    x_average = sum(point[2] * 1/distances[i] for i, point in enumerate(points))/inverse_distances
    y_average = sum(point[3] * 1/distances[i] for i, point in enumerate(points))/inverse_distances
    return x_average, y_average


if __name__ == '__main__':
    print(average_location(k_nearest(-74, -80, 3)))
    print(weighted_average_location(-74, -80, k_nearest(-74, -80, 3)))
