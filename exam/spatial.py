import numpy as np
import math
import xml.etree.ElementTree as ET
import overpass
import random
import pprint
from sklearn.cluster import KMeans, DBSCAN

class Analyzer:

    def __init__(self, location_data):
        self.location_data = location_data

    def preprocess(self, threshold=4):
        self.location_data = self.location_data[self.location_data[:, -1] < threshold]
        self.location_data = self.location_data[self.location_data[:, -2] >= 4]
        return self

    def velocity_pruning(self):
        velocities = np.zeros((len(self.location_data),))
        diffs = np.diff(self.location_data, axis=0)
        for i in range(len(diffs)):
            diff = diffs[i]
            velocity = math.sqrt(diff[0]**2 + diff[1]**2)/diff[2]
            velocities[i] = velocity

        self.location_data = self.location_data[velocities < 1e-5]
        return self

    def cluster(self):
        api = overpass.API()
        calculated_clusters = DBSCAN(eps=1e-3).fit(self.location_data[:, 0:2])
        cluster_labels = np.array(calculated_clusters.labels_)
        for cluster_id in range(max(cluster_labels) + 1):
            points_in_cluster = self.location_data[cluster_labels==cluster_id]
            right_top = points_in_cluster.max(axis=0)
            left_bottom = points_in_cluster.min(axis=0)
            query = overpass.MapQuery(west=left_bottom[0], south=left_bottom[1], east=right_top[0], north=right_top[1])
            pois = api.Get(query)
            print("//////////////////// {} ///////////////////".format(cluster_id))
            # poi_list = [feature for feature in pois.features if(any(good_feature in feature.properties for good_feature in ["tourism", "leisure", "public_transport"]))]
            try:
                poi_name = next(feature.properties for feature in pois.features if("name" in feature.properties and "place" not in feature.properties))
            except StopIteration:
                poi_name = "Unnamed"
            print(poi_name)
            self.generate_basic_kml("cluster{}.kml".format(cluster_id), points_in_cluster)

    def generate_basic_kml(self, destination_filepath, location_data=None):
        root = ET.Element('kml', {"xmlns": "http://www.opengis.net/kml/2.2"})
        tree = ET.ElementTree(root)
        folder = ET.Element('Folder')
        root.append(folder)
        data = location_data if location_data is not None else self.location_data
        for location in data:
            placemark = ET.Element("Placemark")
            point = ET.Element("Point")
            coordinates = self.coordinates_element([location[0:2]])

            point.append(coordinates)
            placemark.append(point)
            folder.append(placemark)
        tree.write(destination_filepath, xml_declaration=True, encoding="UTF-8")

    def coordinates_element(self, coordinate_list):
        coordinates = ET.Element("coordinates")
        coordinates.text = "\n".join(["{},{},0".format(lat, lng) for lat, lng in coordinate_list])
        return coordinates

if __name__ == '__main__':
    location_data = np.genfromtxt("buenosaires.csv", delimiter=',')
    Analyzer(location_data).preprocess().generate_basic_kml("preprocess.kml")
    Analyzer(location_data).preprocess().velocity_pruning().generate_basic_kml("pruning.kml")
    Analyzer(location_data).preprocess().velocity_pruning().cluster()
    Analyzer(location_data).generate_basic_kml("no_preprocess.kml")


