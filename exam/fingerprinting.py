import numpy as np
import math
from scipy.spatial.distance import euclidean


distances = [[0,10.6434,5.5682,19.0625,27.8739],
             [10.6434,0,15.2212,21.8397,29.6435],
             [5.5682,15.2212,0,15.8387,24.5509],
             [19.0625,21.8397,15.8387,0,8.8236],
             [27.8739,29.6435,24.5509,8.8236,0]]

def distance(a, b):
    keys = [45, 47, 51, 80, 96]
    a_index = keys.index(a)
    b_index = keys.index(b)

    return distances[a_index][b_index]

def model(filename):
    return np.genfromtxt(filename, delimiter=',')

def model_hyperbolic(filename):
    data = model(filename)
    hyperbolic_data = []
    for row in data:
        hyperbolic_data.append(to_hyperbolic(row[:-1]) + [row[-1]])
    return hyperbolic_data

def to_hyperbolic(data_point):
    hyperbolic = []
    for i in range(len(data_point)):
        for j in range(i + 1, len(data_point)):
            if data_point[i] == 0 or data_point[j] == 0:
                hyperbolic.append(0)
            else:
                hyperbolic.append(math.log(data_point[i]/data_point[j]))

    return hyperbolic




def predict(model, point):
    most_similar = sorted([(euclidean(row[:-1], point), row[-1]) for row in model])
    most_similar_self_removed = [t for t in most_similar if t[0] != 0]
    most_similar_id_only = [t[1] for t in most_similar_self_removed]

    votes = {}
    for t in most_similar_id_only:
        if t not in votes:
            votes[t] = 1
        else:
            return t

if __name__ == '__main__':
    m = model("exam_train.csv")
    data = np.genfromtxt("exam_train.csv", delimiter=',')
    total_error = 0
    errors = 0
    total = 0
    error_list = []
    for i in range(len(data)):
        prediction = predict(m, data[i, :-1])
        total_error += distance(prediction, data[i, -1])
        if prediction != data[i, -1]:
            errors += 1
            error_list.append(distance(prediction, data[i, -1]))
        total += 1

    print("Accuracy:", total_error/len(data))
    print("Errors/Total:", errors, "/", total)
    print(sorted(error_list))


    errors = 0
    error_list = []
    total = 0
    m = model_hyperbolic("exam_train.csv")
    data = np.genfromtxt("exam_train.csv", delimiter=',')
    total_error = 0
    for i in range(len(data)):
        prediction = predict(m, to_hyperbolic(data[i, :-1]))
        total_error += distance(prediction, data[i, -1])
        if prediction != data[i, -1]:
            errors += 1
            error_list.append(distance(prediction, data[i, -1]))
        total += 1

    print("Accuracy:", total_error/len(data))
    print("Errors/Total:", errors, "/", total)
    print(sorted(error_list))
