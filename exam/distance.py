from math import *
def distance(lat1, lon1, lat2, lon2):
    lat1 = radians(lat1)
    lat2 = radians(lat2)
    lon1 = radians(lon1)
    lon2 = radians(lon2)

    a = 6378137.0
    f = 1/298.257223563
    b = 6356752.314245
    U1 = atan((1- f) * tan(lat1))
    U2 = atan((1- f) * tan(lat2))
    L = lon2 - lon1

    Lambda = L
    for i in range(100):
        sin_sigma = sqrt((cos(U2) * sin(Lambda))**2 + (cos(U1)*sin(U2) - sin(U1)*cos(U2)*cos(Lambda))**2)
        cos_sigma = sin(U1)*sin(U2) + cos(U1)*cos(U2)*cos(Lambda)
        sigma = atan2(sin_sigma, cos_sigma)
        sin_alpha = (cos(U1)*cos(U2)*sin(Lambda))/sin_sigma
        cos2alpha = 1 - sin_alpha**2
        cos2sigmaM = cos_sigma - (2*sin(U1)*sin(U2))/cos2alpha
        C = f/16*cos2alpha*(4 + f*(4 - 3*cos2alpha))
        new_Lambda = L + (1 - C)*f*sin_alpha*(sigma + C*sin_sigma*(cos2sigmaM + C*cos_sigma*(-1 + 2*cos2sigmaM**2)))

        if Lambda == new_Lambda:
            break

        Lambda = new_Lambda


    u2 = cos2alpha*(a**2 - b**2)/b**2
    A = 1 + (u2/16384)*(4096 + u2*(-768 + u2*(320 - 175*u2)))
    B = (u2/1024)*(256 + u2*(-128 + u2*(74 - 47*u2)))
    dsigma = B*sin_sigma*(cos2sigmaM + 0.25*B*(cos_sigma*(-1 + 2*cos2sigmaM**2) - (1/6)*B*cos2sigmaM*(-3 + 4*sin_sigma**2)*(-3 + 4*cos2sigmaM**2)))

    print(b*A*(sigma - dsigma)/1000)


print("Helsinki to Auckland")
distance(-36.8484597, 174.76333150000005, 60.32104159999999, 24.952860399999963)

print("Helsinki to Santiago")
distance(-33.4378305, -70.65044920000003, 60.32104159999999, 24.952860399999963)


