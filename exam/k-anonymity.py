import math
points = [
    (0.2037,0.0394,0.1639),
    (0.2264,0.2426,0.0089),
    (0.0317,0.2393,0.2123),
    (0.2283,0.1213,0.2335),
    (0.1581,0.2001,0.1697),
    (0.0244,0.0355,0.1894),
    (0.0696,0.1054,0.1858),
    (0.1367,0.2289,0.0981),
    (0.2394,0.1981,0.1639),
    (0.2412,0.2399,0.0428)
]
def distance(a, b):
    return math.sqrt((a[0] - b[0])**2 + (a[1] - b[1])**2 + (a[2] - b[2])**2)

for a in points:
    print(sorted([distance(a, b) for b in points])[:4])

d = {}
for i in range(len(points)):
    d[i] = []
    for j in range(len(points)):
        if i == j:
            continue
        if distance(points[i], points[j]) <= 0.15:
            d[i].append(j)

import pprint; pprint.pprint(d)






