from collections import Counter, defaultdict
from itertools import product
import pprint

sequence = "HWSWSHWHSHSWHWHWSWSHSWHSWSSHWHWHWS"
order = 1

products = ["".join(p) for p in product('HWS', repeat=order)]
substrings3 = {ss: {'H': 0, 'W': 0, 'S': 0} for ss in products}
for i in range(order, len(sequence)):
    substrings3[sequence[i-order:i]][sequence[i]] += 1

for key, transitions in substrings3.items():
    count = sum(transitions.values())
    transitions['H'] /= max(count, 1)
    transitions['W'] /= max(count, 1)
    transitions['S'] /= max(count, 1)

pprint.pprint(substrings3)




