import numpy as np
import csv

def kalman():
    with open('noisyCompass.csv') as f:
        data = [np.matrix([[float(r[0])],
                           [float(r[1])],
                           [float(r[2])]]) for r in list(csv.reader(f))]

    sigma = 1.25e-4
    Q = sigma * np.matrix([
        [1, 0, 0],
        [0, 1, 0],
        [1, 0, 1]
    ])
    P = np.matrix([
        [1, 0, 0],
        [0, 1, 0],
        [0, 0, 1]
    ])
    R = 0.15*np.matrix([
        [1, 0, 0],
        [0, 1, 0],
        [1, 0, 1]
    ])

    m = np.matrix([
        [1],
        [1],
        [1]
    ])

    for y in data:
        P_star = P + Q

        m = m + P_star * (P_star + R)**-1 * (y - m)
        P = P_star - P_star * (P_star + R)**-1 * P_star

        print(m)


if __name__ == '__main__':
    kalman()


