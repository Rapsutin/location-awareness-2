import numpy as np
import math
import sys
from datetime import datetime, timezone
np.set_printoptions(suppress=True, linewidth=200)


data = np.genfromtxt("temporal.csv", delimiter=',', skip_header=1)

binned_data = []
for row in data:
    time = row[2]
    timestamp = datetime.fromtimestamp(time, timezone.utc)
    midnight = datetime.combine(timestamp.date(), datetime.min.time().replace(tzinfo=timezone.utc))
    seconds_since_midnight = (timestamp - midnight).seconds

    seconds_in_15_minutes = 60*15
    bin = math.floor(seconds_since_midnight/seconds_in_15_minutes)
    binned_data.append([row[0], row[1], bin])

binned_data = np.array(binned_data)

window_size = 2
sequences = np.array([binned_data[i - window_size : i] for i in range(window_size, len(binned_data))])
similarities = []
for i, sequence in enumerate(sequences[:-window_size]):
    if '--id' not in sys.argv or binned_data[i+1][1] == 2:
        dissimilarity = np.linalg.norm(sequence[:, 2] - sequences[-2][:, 2])**2
        similarities.append((i, binned_data[i+1][2], dissimilarity))

sorted_similarities = np.array(sorted(similarities, key=lambda x: x[2]))

print(np.average(sorted_similarities[:, 1], axis=0, weights=1/sorted_similarities[:, 2]))
print(binned_data[-1])

