import csv
import math
from matplotlib import pyplot as plt
from matplotlib.axes import Axes

THRESHOLDS = [0.00003]
for index, THRESHOLD in enumerate(THRESHOLDS):
    with open('./velocity_pruning.csv') as f:
        data = list(csv.reader(f))[1:]
        for row in data:
            for i in range(len(row)):
                row[i] = float(row[i])


    lat = []
    lng = []

    for i in range(1, len(data)):
        a = data[i-1]
        b = data[i]

        time_difference = b[0] - a[0]
        distance = float(math.sqrt((b[1] - a[1])**2 + (b[2] - a[2])**2))
        speed = distance/time_difference

        if speed <= THRESHOLD:
            lat.append(b[1])
            lng.append(b[2])

    plt.hold(False)
    plt.scatter(lat, lng)
    plt.savefig('{}-{}.png'.format(index, THRESHOLD))

