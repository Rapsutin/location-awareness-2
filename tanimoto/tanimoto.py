import numpy as np
import networkx as nx
from matplotlib import pyplot as plt
from pprint import pprint
from itertools import combinations

def tanimoto(k, l):
    return np.dot(k, l) / (np.linalg.norm(k)**2 + np.linalg.norm(l)**2 - np.dot(k, l))

# RSS vectors

data = np.loadtxt('rssVals.csv', delimiter=',')
fingerprints = nx.Graph()
fingerprints.add_nodes_from(range(len(data)))

for i, j in combinations(range(len(data)), 2):
    similarity = tanimoto(data[i], data[j])
    if similarity > 0.995:
        fingerprints.add_edge(i, j)

nx.draw_spring(fingerprints)
plt.show()

# Response rates

data[data != 0] = 1 # Mark the observed points
response_rates = []
for i in range(1, len(data), 2):
    response_rates.append(data[i-1] + data[i]) # Sum consecutive observations

data = response_rates

fingerprints = nx.Graph()
fingerprints.add_nodes_from(range(len(data)))

for i, j in combinations(range(len(data)), 2):
    similarity = tanimoto(data[i], data[j])
    if similarity > 0.975:
        fingerprints.add_edge(i, j)

nx.draw_spring(fingerprints) # Visual clustering
plt.show()
