import pandas as pd
import matplotlib.pyplot as plt
import math
import numpy as np

if __name__ == '__main__':
    df = pd.read_csv('positioningErrors.csv', header=None)
    errors_1 = df[0].sort_values(inplace=False).reset_index(drop=True)
    errors_2 = df[1].sort_values(inplace=False).reset_index(drop=True)


    cum_dist = np.linspace(0., 100., errors_1.size)

    errors = pd.DataFrame()
    errors['errors 1'] = errors_1
    errors['errors 2'] = errors_2
    errors['percentage'] = cum_dist

    errors.plot(x='percentage', legend=True)
    plt.show()
