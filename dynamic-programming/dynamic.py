import math

word1 = " CARAMEL"
word2 = " GARGAMEL"


def edit(i, j, cost):
    if i == 0:
        return j

    if j == 0:
        return i

    if (word1[i] == word2[j]):
        return edit(i - 1, j - 1, cost)


    return min(edit(i - 1, j, cost), edit(i, j - 1, cost), edit(i - 1, j - 1, cost)) + cost(i, j)

def distance(a, b):
    locations = {
        "C": (0.26, 0.24),
        "A": (0.80, 0.46),
        "R": (0.03, 0.96),
        "M": (0.93, 0.55),
        "E": (0.73, 0.52),
        "L": (0.49, 0.23),
        "G": (0.58, 0.49)
    }
    a_character = word1[a]
    b_character = word2[b]

    a_location = locations[a_character]
    b_location = locations[b_character]

    return math.sqrt((a_location[0] - b_location[0])**2 + (a_location[1] -
                                                           b_location[1])**2)

def constant_cost(a, b):
    return 1

print(" ", *word1)
for j in range(len(word2)):
    print(word2[j], *[edit(i, j, constant_cost) for i in range(len(word1))])

print(edit(len(word1) - 1, len(word2) - 1, distance))
