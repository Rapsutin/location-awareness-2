import csv
import sys
import random

from xml.etree import ElementTree as ET

class LocationData:

    def __init__(self, location_data_filepath, destination_data_filepath):
        with open(location_data_filepath) as f:
            self.location_data = list(csv.reader(f))
        self.destination_data_filepath = destination_data_filepath

        root = ET.Element('kml', {"xmlns": "http://www.opengis.net/kml/2.2"})
        self.tree = ET.ElementTree(root)
        self.folder = ET.Element('Folder')
        root.append(self.folder)

    def generate_basic_kml(self):
        for location in self.location_data:
            placemark = ET.Element("Placemark")
            point = ET.Element("Point")
            coordinates = self.coordinates_element([location])

            point.append(coordinates)
            placemark.append(point)
            self.folder.append(placemark)

    def generate_line_kml(self):
        placemark = ET.Element("Placemark")
        line_string = ET.Element("LineString")

        line_string.append(self.coordinates_element(self.location_data))
        placemark.append(line_string)
        self.folder.append(placemark)

    def add_gaussian_noise(self):
        average = 0
        standard_deviation = 0.01
        self.location_data = [(random.gauss(average, standard_deviation) + float(lat),
                               random.gauss(average, standard_deviation) + float(lng))
                              for lat, lng in self.location_data]


    def coordinates_element(self, coordinate_list):
        coordinates = ET.Element("coordinates")
        coordinates.text = "\n".join(["{},{},0".format(lat, lng) for lat, lng in coordinate_list])
        return coordinates

    def write(self):
        self.tree.write(self.destination_data_filepath, xml_declaration=True, encoding="UTF-8")


if __name__ == '__main__':
    data = LocationData(sys.argv[-2], sys.argv[-1])

    if 'gauss' in sys.argv:
        data.add_gaussian_noise()
    if 'basic' in sys.argv:
        data.generate_basic_kml()
    elif 'line' in sys.argv:
        data.generate_line_kml()

    data.write()

