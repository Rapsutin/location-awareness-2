# CSV to KML tool

Convert a .csv to a .kml by running

`python kml.py {basic|line} [gaussian] <inputfile> <outpufile>`
